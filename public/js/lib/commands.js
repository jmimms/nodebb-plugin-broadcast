(function(Broadcast) {
	var regex = /^\/(\w+)\s?(.+)?/, commandActions = {}, commandInfo = {};

	Broadcast.commands = {
		get: function() {
			return commandInfo;
		},
		register: function(command, commandObj) {
			commandActions[command] = commandObj.handlers.action;
			commandInfo[command] = commandObj.info;
		},
		parse: function(msg, sendBroadcast) {
			var match = msg.match(regex);
			if (match && typeof commandActions[match[1]] === 'function') {
				commandActions[match[1]](match[2] || '', sendBroadcast);
			} else {
				sendBroadcast(msg);
			}
		}
	};
})(window.Broadcast);