(function(Broadcast) {
	var Bug = {
		register: function(shoutPanel) {
			shoutPanel.find('#broadcast-button-bug').off('click').on('click', this.handle);
		},
		handle: function(e) {
			window.open('https://github.com/datzzy/nodebb-plugin-broadcast/issues/new', '_blank').focus();
		}
	};

	Broadcast.actions.register(Bug);
})(window.Broadcast);