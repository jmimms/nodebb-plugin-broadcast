(function(Broadcast) {
	var Hide = {
		register: function(shoutPanel) {
			shoutPanel.off('click', '#broadcast-settings-hide').on('click', '#broadcast-settings-hide', this.handle);
		},
		handle: function(e) {
			var el = $(e.currentTarget).find('span'),
				body = el.parents('#broadcast').find('.panel-body'),
				newSetting;

			if (el.hasClass('fa-arrow-up')) {
				body.slideUp();
				el.removeClass('fa-arrow-up').addClass('fa-arrow-down');
				newSetting = 1;
			} else {
				body.slideDown();
				el.removeClass('fa-arrow-down').addClass('fa-arrow-up');
				newSetting = 0;
			}

			Broadcast.settings.set('toggles.hide', newSetting);
		}
	};

	Broadcast.actions.register(Hide);
})(window.Broadcast);