(function(Broadcast) {
	var Settings = {
		register: function(shoutPanel) {
			shoutPanel.off('click', '#broadcast-settings-menu a').on('click', '#broadcast-settings-menu a', this.handle);
		},
		handle: function(e) {
			var el = $(e.currentTarget),
				statusEl = el.find('span'),
				key = el.data('broadcast-setting'),
				status = statusEl.hasClass('fa-check');

			if (status) {
				statusEl.removeClass('fa-check').addClass('fa-times');
				status = 0;
			} else {
				statusEl.removeClass('fa-times').addClass('fa-check');
				status = 1;
			}

			Broadcast.settings.set(key, status);

			return false;
		}
	};

	Broadcast.actions.register(Settings);
})(window.Broadcast);