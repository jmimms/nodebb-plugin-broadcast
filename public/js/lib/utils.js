(function(Broadcast) {
	var shoutTpl, textTpl,
		sounds;

	require(['sounds'], function(s) {
		sounds = s;
	});

	var Utils = {
		initialize: function(shoutPanel, callback) {
			Broadcast.sockets.initialize();
			Broadcast.actions.initialize(shoutPanel);
			// init with API and look up settings to enable sound
			Broadcast.settings.load(shoutPanel, callback);
		},
		notify: function(data) {
			if (parseInt(Broadcast.settings.get('toggles.notification'), 10) === 1) {
				app.alternatingTitle(Broadcast.vars.messages.alert);
			}
			if (parseInt(Broadcast.settings.get('toggles.sound'), 10) === 1) {
				Broadcast.utils.playSound('notification');
			}
		},
		playSound: function(sound) {
			sounds.playFile('broadcast-' + sound + '.mp3');
		}
	};

	Broadcast.utils = {
		initialize: Utils.initialize,
		notify: Utils.notify,
		playSound: Utils.playSound
	};
})(window.Broadcast);

