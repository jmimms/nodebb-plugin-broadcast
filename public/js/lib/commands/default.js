(function(Broadcast) {
	var SocketMessages = {
		wobble: 'plugins.broadcast.wobble'
	};

	var SocketEvents = {
		onWobble: 'event:broadcast.wobble'
	};

	var ArgumentHandlers = {
		username: function(argument) {
			if (argument.indexOf('@') === 0) {
				argument = argument.slice(1);
			}

			return utils.slugify(argument);
		}
	};

	var DefaultCommands = {
		help: {
			info: {
				usage: '/help',
				description: 'Displays the available commands'
			},
			handlers: {
				action: function(argument, sendShout) {
					var message = '<strong>Available commands:</strong><br>',
						commands = Broadcast.commands.get();

					for (var c in commands) {
						if (commands.hasOwnProperty(c)) {
							message += commands[c].usage + ' - ' + commands[c].description + '<br>';
						}
					}

					Broadcast.utils.showMessage(message);
				}
			}
		},
		wobble: {
			info: {
				usage: '/wobble &lt;username&gt;',
				description: 'WOBULLY SASUGE'
			},
			register: function() {
				Broadcast.sockets.registerMessage('wobble', SocketMessages.wobble);
				Broadcast.sockets.registerEvent(SocketEvents.onWobble, this.handlers.socket);
			},
			handlers: {
				action: function(argument, sendShout) {
					Broadcast.sockets.wobble({
						victim: ArgumentHandlers.username(argument)
					});
				},
				socket: function(data) {
					Broadcast.utils.playSound('wobblysausage');
				}
			}
		},
		thisagain: {
			info: {
				usage: '/thisagain',
				description: 'Remind the n00bs of the obvious'
			},
			handlers: {
				action: function(argument, sendShout) {
					sendShout('This again... Clear your cache and refresh.');
				}
			}
		}
	};

	for (var c in DefaultCommands) {
		if (DefaultCommands.hasOwnProperty(c)) {
			if (DefaultCommands[c].register) {
				DefaultCommands[c].register();
			}
			Broadcast.commands.register(c, DefaultCommands[c]);
		}
	}
})(window.Broadcast);