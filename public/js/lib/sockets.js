(function(Broadcast) {

	var Messages = {
		saveSettings: 'plugins.broadcast.saveSetting',
		getSettings: 'plugins.broadcast.getSettings'
	};

	var Events = {
		onInit: 'event:broadcast.init'
	};

	var Handlers = {
		onInit: function(data) {
			// on broadcast live init display broadcast box
		},
		defaultSocketHandler: function(message) {
			this.message = message;
			var self = this;

			return function (data, callback) {
				if (typeof data === 'function') {
					callback = data;
					data = null;
				}

				socket.emit(self.message, data, callback);
			};
		}
	};

	Broadcast.sockets = {
		messages: Messages,
		events: Events,
		registerMessage: function(handle, message) {
			if (!Broadcast.sockets.hasOwnProperty(handle)) {
				Broadcast.sockets[handle] = new Handlers.defaultSocketHandler(message);
			}
		},
		registerEvent: function(event, handler) {
			if (socket.listeners(event).length === 0) {
				socket.on(event, handler);
			}
		},
		initialize: function() {
			for (var e in Events) {
				if (Events.hasOwnProperty(e)) {
					this.registerEvent(Events[e], Handlers[e]);
				}
			}

			for (var m in Messages) {
				if (Messages.hasOwnProperty(m)) {
					this.registerMessage(m, Messages[m]);
				}
			}
		}
	};
})(window.Broadcast);