(function() {
	$(window).on('action:widgets.loaded', function(e, data) {
		if ($('#broadcast').length > 0) {
			Broadcast.init(data.url);
		}
	});

	window.Broadcast = {
		init: function(url) {
			Broadcast.sockets.initialize();
			Broadcast.settings.load($('#broadcast'), function(){
				Broadcast.actions.initialize($('#broadcast'));
				Broadcast.Ustream = Ustream.UstreamEmbed('UstreamIframe');
				if (Broadcast.settings.get('toggles.sound') == 1 || Broadcast.settings.get('toggles.sound')){
					Broadcast.Ustream.addListener('live', function(){
						Broadcast.utils.playSound('notification');
					});
					Broadcast.Ustream.addListener('offline', function(){
						Broadcast.utils.playSound('wobblysausage');
					});
				}
			});
		}
	};
})();