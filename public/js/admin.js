(function() {
	var settings, wrapper, saveInterval;

	$(document).ready(function() {
		require(['settings'], function(_settings) {
			settings = _settings;

			wrapper = $('#broadcastAdminForm');

			settings.sync('broadcast', wrapper, function() {
				prepareFeatures(settings.get().toggles.features);
			});

			$('#save').click(function(event) {
				event.preventDefault();
				save();
			});

			wrapper.on('change', function(event) {
				save();
			});
		});
	});

	function save() {
		clearTimeout(saveInterval);

		saveInterval = setTimeout(function() {
			settings.persist('broadcast', wrapper, function() {
				socket.emit('admin.plugins.broadcast.sync');
			});
		}, 1000);
	}

	function prepareFeatures(featureSettings) {
		function on(feature) {
			var el = $('[data-feature="' + feature + '"]');
			el.find('.fa').removeClass('fa-times-circle').addClass('fa-check-circle');
			el.removeClass('disabled');

			el.find('input:checkbox').prop('checked', true);
		}

		function off(feature) {
			var el = $('[data-feature="' + feature + '"]');
			el.find('.fa').removeClass('fa-check-circle').addClass('fa-times-circle');
			el.addClass('disabled');

			el.find('input:checkbox').prop('checked', false);
		}

		function toggleFeature(el) {
			var feature = $(el).parents("[data-feature]").data('feature');
			if ($(el).find('.fa').hasClass('fa-times-circle')) {
				on(feature);
			} else {
				off(feature);
			}
			save();
		}

		$('.features').on('click', '.toggle-feature', function() {
			toggleFeature(this);
		}).on('dblclick', '.panel-heading', function() {
			toggleFeature(this);
		}).disableSelection();

		$('.features-save').on('click', function(e) {
			$('#save').click();
			e.preventDefault();
			return false;
		});

		for (var feature in featureSettings) {
			if (featureSettings.hasOwnProperty(feature)) {
				if (!featureSettings[feature]) {
					off(feature);
				} else {
					on(feature);
				}
			}
		}
	}
}());