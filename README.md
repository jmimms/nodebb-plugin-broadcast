# NodeBB Broadcast plugin

This is a NodeBB plugin that will add Ustream broadcasting to your homepage. It's still a work in progress.

Based on [nodebb-plugin-shoutbox](https://github.com/Schamper/nodebb-plugin-shoutbox) by Schamper

## Installation

    npm install nodebb-plugin-broadcast
