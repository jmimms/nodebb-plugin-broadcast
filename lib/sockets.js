var S = require('string'),

	NodeBB = module.require('./nodebb'),
	Config = require('./config'),
	Backend = require('./backend'),
	Commands = require('./commands'),

	SocketIndex = NodeBB.SocketIndex;

//todo clean this up and improve
(function(Sockets) {

	var uidSocketIndex = {},
		handlers = {
		notifyStartTyping: function(socket, data, callback) {
			if (!socket.uid) {
				return;
			}

			var uid = socket.uid;
			SocketIndex.server.sockets.emit('event:broadcast.startTyping', { uid: uid });

			if (socket.listeners('disconnect').length === 0) {
				socket.on('disconnect', function() {
					Sockets.notifyStopTyping(socket, data, callback);
				});
			}
		},
		notifyStopTyping: function(socket, data, callback) {
			if (!socket.uid) {
				return;
			}

			var uid = socket.uid;
			SocketIndex.server.sockets.emit('event:broadcast.stopTyping', { uid: uid });
		},
		getSettings: Config.userSockets.getSettings,
		saveSetting: Config.userSockets.saveSettings
	};

	function updateUidSocketIndex(socket) {
		if (!socket.isBot) {
			uidSocketIndex[socket.uid] = socket;
			if (socket.listeners('disconnect').length === 0) {
				socket.on('disconnect', function() {
					delete uidSocketIndex[socket.uid];
				});
			}
		}
	}

	for (var s in Commands.sockets) {
		if (Commands.sockets.hasOwnProperty(s)) {
			handlers[s] = Commands.sockets[s];
		}
	}

	Sockets.events = handlers;
	Sockets.uidSocketIndex = uidSocketIndex;

})(module.exports);