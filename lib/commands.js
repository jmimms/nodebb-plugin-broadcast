var NodeBB = module.require('./nodebb'),
	Sockets = require('./sockets'),
	User = NodeBB.User,

	SocketIndex = NodeBB.SocketIndex;

(function(Commands) {
	Commands.sockets = {
		wobble: function(socket, data, callback) {
			if (!socket.uid || !data) {
				return;
			}

			if (data.victim && data.victim !== '') {
				User.getUidByUserslug(data.victim, function(err, uid) {
					if (err) {
						return callback(err);
					}

					if (uid) {
						var userSocket = Sockets.uidSocketIndex[uid];
						if (userSocket) {
							userSocket.emit('event:broadcast.wobble');
						}
					}
				});
			} else {
				socket.emit('event:broadcast.wobble');
			}
		}
	};
})(module.exports);