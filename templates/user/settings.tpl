<div class="checkbox">
    <label><input type="checkbox" data-property="broadcast:toggles:hide"> Hide broadcast</label>
</div>
<div class="checkbox">
    <label><input type="checkbox" data-property="broadcast:toggles:sound"> Play a sound on a live broadcast</label>
</div>
<div class="checkbox">
    <label><input type="checkbox" data-property="broadcast:toggles:notification"> Display a notification in the titlebar on a live broadcast</label>
</div>