<div class="row">
    <div class="col-md-12">
        <h1>Broadcast</h1>
    </div>
</div>

<div class="row">
    <form class="form" id="broadcastAdminForm">
        <div class="col-xs-6 pull-left">
            <h3>Settings
                <small>change settings</small>
            </h3>

            <hr>

            <div class="form-group">
                <label for="ustream">Ustream ID:</label>
                <input type="text" data-key="toggles.ustreamId" data-empty="false" data-trim="false" class="form-control" id="ustream" placeholder="ID" value="{ustreamId}">
            </div>
            
            <div class="form-group">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" data-key="toggles.soundAlerts" data-empty="false" data-trim="false"> Sound alerts on live
                    </label>
                </div>
            </div>
        </div>
    </form>
    <div class="col-xs-12">
        <hr>

        <div class="pull-left">
            <small>
                Settings are saved automatically but here's a save button just in case
            </small>
            <br>
            <button id="save" class="btn btn-success btn-xs pull-right">
                Save
            </button>
        </div>
    </div>
</div>

<script src="/plugins/nodebb-plugin-shoutbox/public/js/admin.js"></script>